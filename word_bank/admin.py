from django.contrib import admin
from .models import Word,Meaning
# Register your models here.


class MeaningInLine(admin.TabularInline):
    model = Meaning
    extra = 3


class WordAdmin(admin.ModelAdmin):
    fields = ['word_text','search_count']
    inlines = [MeaningInLine]


admin.site.register(Word,WordAdmin)
