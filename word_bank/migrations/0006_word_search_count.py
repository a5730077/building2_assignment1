# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-02 06:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('word_bank', '0005_auto_20160301_1615'),
    ]

    operations = [
        migrations.AddField(
            model_name='word',
            name='search_count',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
    ]
