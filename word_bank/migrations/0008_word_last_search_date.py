# -*- coding: utf-8 -*-
# Generated by Django 1.9.4 on 2016-05-02 07:36
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('word_bank', '0007_auto_20160502_0722'),
    ]

    operations = [
        migrations.AddField(
            model_name='word',
            name='last_search_date',
            field=models.DateTimeField(default=datetime.datetime(2016, 5, 2, 7, 36, 45, 566676, tzinfo=utc), verbose_name='date published'),
            preserve_default=False,
        ),
    ]
