from django.db import models

# Create your models here.
class Word(models.Model):
    word_text = models.CharField(max_length = 200)
    search_count = models.IntegerField(default=0)
    last_search_date = models.DateTimeField('date published')
    def __str__(self):
        return self.word_text

class Meaning(models.Model):
    word = models.ForeignKey(Word, on_delete=models.CASCADE)
    meaning_text = models.CharField(max_length = 200)
    meaning_type = models.CharField(max_length = 10)
    sample_sentence_text = models.CharField(max_length = 200)

    def __str__(self):
        return '({}) '.format(self.meaning_type)+self.meaning_text+' : '+self.sample_sentence_text


