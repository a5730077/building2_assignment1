from django.conf.urls import  url
from . import views

app_name = 'word_bank'
urlpatterns = [
    url(r'^$',views.index , name = 'index'),
    url(r'^find_word/$',views.find_word, name = 'find_word'),
    url(r'^add_word_page/$',views.add_word_page, name = 'add_word_page'),
    url(r'^show_all_word/$',views.show_all_word, name = 'show_all_word'),
    url(r'^(?P<word_text>[a-z]+)/detail/$',views.detail, name='detail'),
    url(r'^save_load_csv_page/$',views.save_load_csv_page, name='save_load_csv_page'),
    url(r'^load_csv_file/$',views.load_csv_file, name='load_csv_file'),
    url(r'^save_csv_file/$',views.save_csv_file, name='save_csv_file'),
    url(r'^save_xml_file/$', views.save_xml_file, name='save_xml_file'),
    url(r'^load_xml_file/$', views.load_xml_file, name='load_xml_file'),
    url(r'^write_upload_file_for_load_word/$', views.write_upload_file_for_load_word, name='write_upload_file_for_load_word'),
    url(r'^standard_load_xml_file/$', views.standard_load_xml_file, name='standard_load_xml_file'),
    url(r'^standard_save_xml_file/$', views.standard_save_xml_file, name='standard_save_xml_file'),


]