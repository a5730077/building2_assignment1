from django.apps import AppConfig


class WordBankConfig(AppConfig):
    name = 'word_bank'
