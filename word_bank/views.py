from django.shortcuts import render,get_object_or_404
from word_bank.models import Word
from django.http import HttpResponseRedirect,HttpResponse
from django.core.urlresolvers import reverse
from django.core.files.storage import default_storage
import csv
from django.utils.encoding import smart_str
import os
from django.contrib.staticfiles.templatetags.staticfiles import static
import xml.etree.ElementTree as ET
from django.utils import timezone

# Create your views here.
def index(request):
    request.session['load'] = ''
    all_word = Word.objects.order_by('-search_count')[:4]  # get object is search by user often only 4 words
    last_words = Word.objects.order_by('-last_search_date')[:4]  # get objects is search lastest only 4 words
    context = {'all_word':all_word,'last_words':last_words}
    return render(request,'word_bank/index.html',context)

def find_word(request):
    word_search = request.GET['word']
    words = Word.objects.filter(word_text__startswith = word_search)  # return list of objects words
    if (len(words) == 1) and (words[0].word_text == word_search):  # have only 1 word and word_text match go to detail page
        return HttpResponseRedirect(reverse('word_bank:detail',kwargs={'word_text': words[0].word_text }))
    else:  # return more 1 words
        context = {'words':words,'word_search':word_search}
        return render(request,'word_bank/find_word.html',context)


def add_word_page(request):
    error_message = ''  # for display error message on page
    if request.method == 'POST':
        word_text_form = request.POST['word']
        meaning_text_form = request.POST['meaning']
        meaning_type_text_form = request.POST['meaning_type']
        sample_sentence_text_form = request.POST['sample_sentence']

        if Word.objects.filter(word_text=word_text_form):  # do if have any objects same name
            word = Word.objects.get(word_text=word_text_form)
            if not word.meaning_set.filter(meaning_text=meaning_text_form):  # if not have same meaning , create new meaning
                word.meaning_set.create(meaning_text=meaning_text_form,meaning_type=meaning_type_text_form,sample_sentence_text=sample_sentence_text_form)
                error_message = 'add new your meaning complete'
            else:
                error_message = 'have same meaning already'

        else:  # not same name can create new word
            word = Word(word_text=word_text_form)
            word.save()
            word.meaning_set.create(meaning_text=meaning_text_form,meaning_type=meaning_type_text_form,sample_sentence_text=sample_sentence_text_form)
            error_message = 'Add Your New Word Completely'
    context = {'error_message':error_message}
    return render(request,'word_bank/add_word_page.html',context)

def show_all_word(request):
    all_word = Word.objects.order_by('word_text')
    context = {'all_word':all_word}
    return render(request,'word_bank/show_all_word.html',context)

def detail(request, word_text):
    word = get_object_or_404(Word, word_text=word_text)  # return word object or 404
    word.search_count = word.search_count + 1  # plus search counter by 1 for measument frequently
    word.last_search_date = timezone.now()  # set last_search_date is timezone now
    word.save()
    print(word.search_count)
    context = {'word':word}
    return render(request,'word_bank/detail.html',context)

def save_load_csv_page(request):
    return render(request,'word_bank/save_load_csv_page.html')

def load_csv_file(request):
    #dirPath = 'word_bank/static/word_bank/load_csv/'
    #if request.FILES != {}:  # if have file uploaded
        #csvFile = request.FILES['csvFile']
        #saveFilePath = default_storage.save(dirPath,csvFile)
        #os.rename(saveFilePath,'{}{}'.format(dirPath,csvFile.name))
    all_word = Word.objects.all()
    for word in all_word:
        word.delete()  # delete all before read word in csv
    with open(request.session['load']) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if row['word'] != '':
                if Word.objects.filter(word_text=row['word']):
                    word = Word.objects.get(word_text=row['word'])
                else:
                    word = Word(word_text=row['word'])
                    word.save()
            else:
                if not word.meaning_set.filter(meaning_text=row['meaning']):  # if not have same meaning , create new meaning
                    word.meaning_set.create(meaning_text=row['meaning'], meaning_type=row['type'], sample_sentence_text=row['sample_sentence'])
    request.session['load'] = 'Words have been loaded successfully!'  # show loaded messeage at save_load_csv_file page
    return HttpResponseRedirect(reverse('word_bank:save_load_csv_page'))
    #else:
       # request.session['load'] = 'No File To Upload'
        #return HttpResponseRedirect(reverse('word_bank:save_load_csv_page'))  # not read file if haven't file uploaded

def load_xml_file(request):
    all_word = Word.objects.all()
    for word in all_word:
        word.delete()  # delete all before read word in xml
    tree = ET.parse(request.session['load'])  # read file form backup_xml
    words = tree.getroot()
    for et_word in words:
        if Word.objects.filter(word_text=et_word.attrib['word_text']):  # check name object is same already or not
            word = Word.objects.get(word_text=et_word.attrib['word_text'])
        else:
            word = Word(word_text=et_word.attrib['word_text'])
            word.save()
        # add meaning and sample sentence
        for et_meaning in et_word.findall('meaning'):
            if not word.meaning_set.filter(meaning_text=et_meaning.attrib['meaning_text']):
                et_meaning_type = et_meaning.find('meaning_type').text
                et_sample_sentence_text = et_meaning.find('sample_sentence_text').text
                word.meaning_set.create(meaning_text=et_meaning.attrib['meaning_text'], meaning_type=et_meaning_type, sample_sentence_text=et_sample_sentence_text)
    request.session['load'] = 'Words have been loaded successfully!'  # show loaded messeage at save_load_csv_file page
    return HttpResponseRedirect(reverse(('word_bank:save_load_csv_page')))

def standard_load_xml_file(request):
    all_word = Word.objects.all()
    for word in all_word:
        word.delete()

    tree = ET.parse(request.session['load'])
    words = tree.getroot()
    for et_word in words:
        if Word.objects.filter(word_text=et_word.find('text').text):  # check name objects is same already or not
            word = Word.objects.get(word_text=et_word.find('text').text)
        else:
            word = Word(word_text=et_word.find('text').text)
            word.save()
        if not word.meaning_set.filter(meaning_text=et_word.find('mean').text):
            word.meaning_set.create(meaning_text=et_word.find('mean').text, meaning_type=et_word.find('type').text, sample_sentence_text=et_word.find('ex').text)
    request.session['load'] = 'Words have been loaded successfully'  # show loaded messaged at save load_csv_file page
    return HttpResponseRedirect(reverse('word_bank:save_load_csv_page'))

def write_upload_file_for_load_word(request):
    if request.FILES != {}:  # if have file uploaded
        file = request.FILES['file']
        file_type = request.POST['file_type']
        if ('.'+file_type) not in file.name:  # file type not match with type file is uploaded
            request.session['load'] = 'type file is uploaded not match'
            return HttpResponseRedirect(reverse('word_bank:save_load_csv_page'))
        else:                                 # file type match with type file is uploaded
            if file_type == 'csv':
                dirPath = 'word_bank/static/word_bank/load_csv/'
                saveFilePath = default_storage.save(dirPath, file)
                os.rename(saveFilePath, '{}{}'.format(dirPath, file.name))
                request.session['load'] = '{}{}'.format(dirPath, file.name)
                return HttpResponseRedirect(reverse('word_bank:load_csv_file'))
            elif file_type == 'xml':
                dirPath = 'word_bank/static/word_bank/load_xml/'
                saveFilePath = default_storage.save(dirPath, file)
                os.rename(saveFilePath, '{}{}'.format(dirPath, file.name))
                request.session['load'] = '{}{}'.format(dirPath, file.name)
                #return HttpResponseRedirect(reverse('word_bank:load_xml_file'))
                return HttpResponseRedirect(reverse('word_bank:standard_load_xml_file'))
    else:
        request.session['load'] = 'No File To Upload'
        return HttpResponseRedirect(reverse('word_bank:save_load_csv_page'))  # not read file if haven't file uploaded

def save_csv_file(request):
    if request.GET.get('sort', '') :  # sort a-z
        all_word = all_word = Word.objects.order_by('word_text')
    else:
        all_word = Word.objects.all()
    fieldnames = ['word','type','meaning','sample_sentence']
    output = csv.DictWriter(open('word_bank/static/word_bank/backup_csv/word.csv','w'), fieldnames=fieldnames)
    output.writeheader()
    for word in all_word:
        output.writerow({'word':word.word_text})
        for meaning in word.meaning_set.all():
            meaning_text = smart_str(meaning.meaning_text)
            output.writerow({'word': '', 'type': meaning.meaning_type, 'meaning': meaning_text,'sample_sentence': meaning.sample_sentence_text})
    url = static('word_bank/backup_csv/word.csv')
    return HttpResponseRedirect(url)

def save_xml_file(request):
    if request.GET.get('sort', ''):  # sort a-z
        all_word = all_word = Word.objects.order_by('word_text')
    else:
        all_word = Word.objects.all()
    #request.session['load'] = 'save xml file'
    words = ET.Element('words')
    for word in all_word:
        et_word = ET.SubElement(words,'word',word_text = word.word_text)
        for meaning in word.meaning_set.all():
            et_meaning = ET.SubElement(et_word,'meaning',meaning_text=meaning.meaning_text)
            ET.SubElement(et_meaning,'meaning_type').text = meaning.meaning_type
            ET.SubElement(et_meaning,'sample_sentence_text').text = meaning.sample_sentence_text
    # create xml file
    tree = ET.ElementTree(words)
    tree.write('word_bank/static/word_bank/backup_xml/word.xml')

    #return HttpResponseRedirect(reverse('word_bank:save_load_csv_page'))
    url = static('word_bank/backup_xml/word.xml')
    return HttpResponseRedirect(url)

def standard_save_xml_file(request):
    if request.GET.get('sort',''):  # sort a-z
        all_word = all_word = Word.objects.order_by('word_text')
    else:
        all_word = Word.objects.all()
    words = ET.Element('bank')
    for word in all_word:
        for meaning in word.meaning_set.all():
            et_word = ET.SubElement(words, 'word')
            ET.SubElement(et_word,'text').text = word.word_text
            ET.SubElement(et_word,'type').text = meaning.meaning_type
            ET.SubElement(et_word,'mean').text = meaning.meaning_text
            ET.SubElement(et_word,'ex').text = meaning.sample_sentence_text
    tree = ET.ElementTree(words)
    tree.write('word_bank/static/word_bank/backup_xml/standard_backup_word.xml')
    url = static('word_bank/backup_xml/standard_backup_word.xml')
    return HttpResponseRedirect(url)




