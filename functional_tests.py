from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest

class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_can_open_home_page_and_go_to_word_bank_app(self):
        #Eddy has heard about a cool word form his friend and want to know meaning.
        #He goes to check out its homepage
        self.browser.get('http://localhost:8000')

        #He notices the page title and header mention home page
        self.assertIn('Assignment',self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('My Home Page', header_text)

        #He see the link to app word bank online
        link_word_bank_app = self.browser.find_element_by_link_text('App Word Bank Online')
        self.assertIn('App Word Bank Online',link_word_bank_app.text)

        #He clink app word bank online link
        link_word_bank_app.click()

        # He saw the header is Welcom to Word Bank Online
        header_element = self.browser.find_element_by_tag_name('h1')
        self.assertIn('Welcome to Word Bank Online',header_element.text)

    def test_can_type_word_to_find_and_go_to_detail_page_when_word_match_only_1_word(self):

        #He goes to check out its homepage and go to app word bank link
        self.browser.get('http://localhost:8000')
        link_word_bank_app = self.browser.find_element_by_link_text('App Word Bank Online')
        link_word_bank_app.click()

        #He saw box to type find the word
        inputbox = self.browser.find_element_by_name('word')
        # He types beam in to input box and pressed enter
        inputbox.send_keys('beam')
        inputbox.send_keys(Keys.ENTER)

        # the page link to word detail page and he saw header page is Word Detail
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn('Word Detail',header_text)

        # the page display find word : in <b> tag
        find_word_text = self.browser.find_element_by_tag_name('b').text
        self.assertIn('Find Word :',find_word_text)

    def test_can_go_to_word_lists_when_type_word_to_find_and_match_more_1_word_or_no_send_word_to_find(self):
        #He goes to check out its homepage and go to app word bank link
        self.browser.get('http://localhost:8000')
        link_word_bank_app = self.browser.find_element_by_link_text('App Word Bank Online')
        link_word_bank_app.click()

        #He saw box to type find the word
        inputbox = self.browser.find_element_by_name('word')
        # He types beam in to input box and pressed enter
        inputbox.send_keys('t')
        inputbox.send_keys(Keys.ENTER)

        # He saw header is words list
        header = self.browser.find_element_by_tag_name('h1')
        self.assertIn('Words list',header.text)

        # He see the link of tennis he clink it
        tennis_link = self.browser.find_element_by_link_text('tennis')
        self.assertIn('tennis',tennis_link.text)
        tennis_link.click()

        # the page update to detail link of word tennis
        current_url = self.browser.current_url
        self.assertIn('tennis',current_url)

        # He go back to find word page
        self.browser.back()
        self.browser.back()

        # He not type anything in inputbox and pressed enter
        inputbox = self.browser.find_element_by_name('word')
        inputbox.clear()
        inputbox.send_keys(Keys.ENTER)

        # He see the text on page is No Results
        text_on_word_list_page = self.browser.find_elements_by_tag_name('b')
        self.assertIn('No Results',[word.text for word in text_on_word_list_page])


    def test_can_go_to_show_all_word_page_and_go_to_detial_page_by_link(self):

        #He goes to check out its homepage and go to app word bank link
        self.browser.get('http://localhost:8000')
        link_word_bank_app = self.browser.find_element_by_link_text('App Word Bank Online')
        link_word_bank_app.click()

        # He see the link show all word
        link_show_all_word = self.browser.find_element_by_link_text('Show All Word')
        self.assertIn('Show All Word',link_show_all_word.text)

        # he clink show all word link
        link_show_all_word.click()

        # He saw header is Show All Word
        header = self.browser.find_element_by_tag_name('h1')
        self.assertIn('Show All Word',header.text)

        # he saw a lot of word link and he clink at banana link
        banana_link = self.browser.find_element_by_link_text('banana')
        banana_link.click()

        # the page update to Word Detail page and current url have the text is banana
        header = self.browser.find_element_by_tag_name('h1')
        self.assertIn('Word Detail',header.text)
        current_url = self.browser.current_url
        self.assertIn('banana',current_url)

        self.fail('Finish the test')

if __name__ == '__main__':
    unittest.main(warnings='ignore')
